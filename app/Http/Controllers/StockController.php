<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Alert;
use Validator;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stuff = DB::table('stuffs')->orderBy('name', 'asc')->get();
        $data = DB::table('stocks as a')
                    ->select('a.*', 'b.name', 'b.code')
                    ->leftJoin('stuffs as b', 'a.stuff_id', 'b.id')
                    ->orderBy('id', 'asc')
                    ->get();
        return view('stock.index', compact('data', 'stuff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'stuff' => 'required',
            'qty' => 'required',
            'status' => 'required'
        ]);
        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $check = DB::table('stocks')->where('stuff_id', $request->stuff)->where('status', $request->status)->count();
                    if ($check >= 1) {
                        DB::table('stocks')->where('stuff_id', $request->stuff)->where('status', $request->status)->increment('total', $request->qty);
                        Alert::success('Berhasil', 'Stok Berhasil Ditambahkan');
                        return redirect()->back();
                    } else {
                        DB::table('stocks')->insert([
                            'stuff_id' => $request->stuff,
                            'total' => $request->qty,
                            'status' => $request->status,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                        Alert::success('Berhasil', 'Stok Berhasil Dibuat');
                        return redirect()->back();
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Terjadi Kesalahan', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'qty' => 'required'
        ]);
        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request, $id) {
                try {
                    DB::table('stocks')->where('id', base64_decode($id))->update([
                        'total' => $request->qty,
                    ]);
                    Alert::success('Berhasil', 'Stok Berhasil Diupdate');
                    return redirect()->back();
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Terjadi Kesalahan', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = DB::table('stocks')->where('id', base64_decode($id))->delete();
        if ($del) {
            Alert::success('Berhasil', 'Stok Barang Berhasil Dihapus');
            return redirect()->back();
        } else {
            Alert::error('Terjadi Kesalahan', 'Stok Barang Gagal Dihapus');
            return redirect()->back();
        }
    }
}
