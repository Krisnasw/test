<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Validator;
use DB;
use Alert;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('transactions')->orderBy('id', 'asc')->get();
        return view('transaction.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'type' => 'required'
        ]);
        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    DB::table('transactions')->insert([
                        'code' => IdGenerator::generate(['table' => 'transactions', 'field' => 'code', 'length' => 10, 'prefix' =>'TRX-']),
                        'name' => $request->name,
                        'phone' => $request->phone,
                        'address' => $request->address,
                        'type' => $request->type,
                        'total' => 0,
                        'description' => $request->description,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    $lastId = DB::getPdo()->lastInsertId();
                    Alert::success('Berhasil', 'Transaksi Berhasil Dibuat');
                    return redirect('home/transaction/' . base64_encode($lastId));
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                } 
            });
            return $post;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = DB::table('transactions')->where('id', base64_decode($id))->first();
        $item = DB::table('transaction_items as a')
                    ->select('a.*', 'b.name', 'b.code as stuffCode')
                    ->leftJoin('stuffs as b', 'a.stuff_id', 'b.id')
                    ->where('a.transaction_id', base64_decode($id))
                    ->get();
        $stuff = DB::table('stocks as a')
                    ->select('a.*', 'b.name', 'b.code')
                    ->leftJoin('stuffs as b', 'a.stuff_id', 'b.id')
                    ->where('a.status', 'in')
                    ->orderBy('b.name', 'asc')
                    ->get();
        return view('transaction.show', compact('data', 'item', 'stuff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'type' => 'required'
        ]);
        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request, $id) {
                try {
                    DB::table('transactions')->where('id', base64_decode($id))->update([
                        'name' => $request->name,
                        'phone' => $request->phone,
                        'address' => $request->address,
                        'type' => $request->type,
                        'description' => $request->description
                    ]);
                    Alert::success('Berhasil', 'Transaksi Berhasil Diupdate');
                    return redirect()->back();
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                } 
            });
            return $post;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = DB::table('transactions')->where('id', base64_decode($id))->delete();
        if ($del) {
            Alert::success('Berhasil', 'Transaksi Berhasil Dihapus');
            return redirect()->back();
        } else {
            Alert::error('Error', 'Transaksi Gagal Dihapus');
            return redirect()->back();
        }
    }

    public function storeTrxItem(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'trx' => 'required',
            'stuff' => 'required',
            'qty' => 'required',
            'price' => 'required'
        ]);
        if ($valid->fails()) {
            Alert::info('Info', $valid->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $check = DB::table('transaction_items')->where('transaction_id', $request->trx)->where('stuff_id', $request->stuff)->count();
                    if ($check >= 1) {
                        $data = DB::table('transaction_items')->where('transaction_id', $request->trx)->where('stuff_id', $request->stuff)->first();
                        DB::table('transaction_items')->where('id', $data->id)->update([
                            'qty' => $request->qty,
                            'price' => $request->price,
                            'total' => $request->qty * $request->price
                        ]);
                        DB::table('stocks')->where('stuff_id', $request->stuff)->where('status', 'in')->decrement('total', $request->qty);
                        Alert::success('Berhasil', 'Barang Berhasil Ditambahkan');
                        return redirect()->back();
                    } else {
                        DB::table('transaction_items')->insert([
                            'transaction_id' => $request->trx,
                            'stuff_id' => $request->stuff,
                            'qty' => $request->qty,
                            'price' => $request->price,
                            'total' => $request->qty * $request->price
                        ]);
                        DB::table('stocks')->where('stuff_id', $request->stuff)->where('status', 'in')->decrement('total', $request->qty);
                        Alert::success('Berhasil', 'Barang Berhasil Ditambahkan');
                        return redirect()->back();
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }

    public function deleteTrxItem($id)
    {
        $data = DB::table('transaction_items')->where('id', base64_decode($id))->first();
        DB::table('stocks')->where('stuff_id', $data->stuff_id)->where('status', 'in')->increment('total', $data->qty);
        $del = DB::table('transaction_items')->where('id', base64_decode($id))->delete();
        if ($del) {
            Alert::success('Berhasil', 'Barang Berhasil Dihapus');
            return redirect()->back();
        } else {
            Alert::error('Error', 'Gagal Menghapus Barang');
            return redirect()->back();
        }
    }

    public function doVerified($id)
    {
        $total = DB::table('transaction_items')->where('transaction_id', base64_decode($id))->sum('total');
        $data = DB::table('transactions')->where('id', base64_decode($id))->update([
            'isLocked' => true,
            'total' => $total
        ]);
        if ($data) {
            Alert::success('Berhasil', 'Berhasil Verifikasi Transaksi');
            return redirect()->back();
        } else {
            Alert::error('Error', 'Gagal Verifikasi Transaksi');
            return redirect()->back();
        }
    }
}
