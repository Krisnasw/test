<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use DB;
use Validator;
use Alert;
use File;

class StuffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('stuffs')->orderBy('name', 'asc')->get();
        return view('stuff.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png'
        ]);
        if ($valid->fails()) {
            return response(['error' => true, 'message' => $valid->errors()->first()], 500);
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    DB::table('stuffs')->insert([
                        'code' => IdGenerator::generate(['table' => 'stuffs', 'field' => 'code', 'length' => 10, 'prefix' =>'BRG-']),
                        'name' => $request->name,
                        'image' => Self::savePhoto($request->file('image')),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    return response(['error' => false, 'message' => 'Barang berhasil ditambahkan'], 200);
                } catch (\Exception $e) {
                    DB::rollback();
                    return response(['error' => true, 'message' => $e->getMessage()], 500);
                }
            });
            return $post;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($valid->fails()) {
            return response(['error' => true, 'message' => $valid->errors()->first()], 500);
        } else {
            $post = DB::transaction(function () use ($request, $id) {
                try {
                    $data = DB::table('stuffs')->where('id', base64_decode($id))->first();
                    DB::table('stuffs')->where('id', base64_decode($id))->update([
                        'name' => $request->name,
                        'image' => ($request->hasFile('image') ? Self::deletePhoto($data->image) && Self::savePhoto($request->file('image')) : $data->image),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    return response(['error' => false, 'message' => 'Barang berhasil diupdate'], 200);
                } catch (\Exception $e) {
                    DB::rollback();
                    return response(['error' => true, 'message' => $e->getMessage()], 500);
                }
            });
            return $post;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = DB::table('stuffs')->where('id', base64_decode($id))->delete();
        if ($del) {
            return response(['error' => false, 'message' => 'Berhasil Menghapus Barang'], 200);
        } else {
            return response(['error' => true, 'message' => 'Gagal Menghapus Barang'], 200);
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'img';
        $subdestinationPath = 'stuff';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;
        return $data['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
