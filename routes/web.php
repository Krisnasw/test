<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'home'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('stuff', 'StuffController')->except(['create', 'show', 'edit']);
    Route::resource('stock', 'StockController')->except(['create', 'show', 'edit']);
    Route::resource('transaction', 'TransactionController');
    Route::post('transaction/item', 'TransactionController@storeTrxItem')->name('transaction.item.store');
    Route::delete('transaction/item/delete/{id}', 'TransactionController@deleteTrxItem')->name('transaction.item.destroy');
    Route::get('transaction/verify/{id}', 'TransactionController@doVerified')->name('transaction.verify');
});
