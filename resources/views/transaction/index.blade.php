@extends('layouts.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{!! asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') !!}">
<link rel="stylesheet" href="{!! asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') !!}">
<link rel="stylesheet" href="{!! asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') !!}">
<link rel="stylesheet" href="{!! asset('plugins/select2/css/select2.min.css') !!}">
@stop
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">List Transaksi</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-sm btn-outline-primary btn-block" data-toggle="modal" data-target="#modal-primary">
                <i class="fa fa-plus"></i>
                Tambah Data
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Deskripsi</th>
                    <th>Tipe</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Dibuat Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php $no = 1; @endphp
                @foreach($data as $row)
                <tr>
                    <td>{!! $no++ !!}</td>
                    <td>{!! $row->code !!}</td>
                    <td>{!! $row->description !!}</td>
                    <td align="center">
                        @if($row->type == 'sell')
                        <span class="badge bg-primary">Penjualan</span>
                        @else
                        <span class="badge bg-danger">Pembelian</span>
                        @endif
                    </td>
                    <td>Rp {!! number_format($row->total) !!}</td>
                    <td align="center">
                        @if($row->isLocked)
                        <span class="badge bg-success">Verified</span>
                        @else
                        <span class="badge bg-danger">Draft</span>
                        @endif
                    </td>
                    <td>{!! date('Y-m-d H:i:s', strtotime($row->created_at)) !!}</td>
                    <td align="center">
                        <div class="btn-group">
                            <a href="{!! route('transaction.show', base64_encode($row->id)) !!}" type="button" class="btn btn-warning">
                              <i class="fas fa-eye"></i>
                            </a>
                            @if(!$row->isLocked)
                            <a href="{!! route('transaction.verify', base64_encode($row->id)) !!}" type="button" class="btn btn-info">
                              <i class="fas fa-check"></i>
                            </a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modDelete{!! $row->id !!}">
                              <i class="fas fa-trash"></i>
                            </button>
                            @endif
                        </div>
                    </td>
                </tr>

                <!-- /.card -->
                <div class="modal fade" id="modDelete{!! $row->id !!}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Hapus Data</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah kamu yakin akan menghapus data?</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                                <form method="POST" action="{!! route('transaction.destroy', base64_encode($row->id)) !!}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Deskripsi</th>
                    <th>Tipe</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Dibuat Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
    <!-- /.card -->
    <div class="modal fade" id="modal-primary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{!! route('transaction.store') !!}">
                        @csrf
                        <div class="card-body">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputPassword3" name="name" placeholder="Masukkan Nama Pembeli">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">No. Telp</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputPassword3" name="phone" placeholder="Masukkan No. Telp Pembeli">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputPassword3" name="address" placeholder="Masukkan Alamat Pembeli">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Tipe</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="type">
                                    <option value="-">Pilih Tipe</option>
                                    <option value="sell">Penjualan</option>
                                    <option value="buy">Pembelian</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Catatan</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" placeholder="Masukkan catatan">
                                    
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Submit</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
@stop
@section('script')
<!-- DataTables  & Plugins -->
<script src="{!! asset('plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('plugins/jszip/jszip.min.js') !!}"></script>
<script src="{!! asset('plugins/pdfmake/pdfmake.min.js') !!}"></script>
<script src="{!! asset('plugins/pdfmake/vfs_fonts.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.colVis.min.js') !!}"></script>
<script src="{!! asset('plugins/select2/js/select2.full.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('.select2').select2();
    });
</script>
@stop
