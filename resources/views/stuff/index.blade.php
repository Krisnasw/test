@extends('layouts.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{!! asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') !!}">
<link rel="stylesheet" href="{!! asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') !!}">
<link rel="stylesheet" href="{!! asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') !!}">
@stop
@section('content')
<div class="card" id="app">
    <div class="card-header">
        <h3 class="card-title">List Barang</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-sm btn-outline-primary btn-block" data-toggle="modal" data-target="#modal-primary">
                <i class="fa fa-plus"></i> 
                Tambah Data
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Nama Barang</th>
                    <th>Foto</th>
                    <th>Dibuat Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php $no = 1; @endphp
                @foreach($data as $row)
                <tr>
                    <td>{!! $no++ !!}</td>
                    <td>{!! $row->code !!}</td>
                    <td>{!! $row->name !!}</td>
                    <td>
                        <a href="{!! asset($row->image) !!}" target="_blank">Lihat</a>
                    </td>
                    <td>{!! date('d, F Y', strtotime($row->created_at)) !!}</td>
                    <td align="center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modUpdate{!! $row->id !!}">
                              <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger" @click.prevent="deleteData('{{ base64_encode($row->id) }}')">
                              <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>

                <!-- /.card -->
                <div class="modal fade" id="modUpdate{!! $row->id !!}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Ubah Data Barang</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- form start -->
                                <form class="form-horizontal" enctype="multipart/form-data" @submit.prevent="updateData('{!! base64_encode($row->id) !!}')">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputEmail31" v-model="form.update.name" value="{!! $row->name !!}" name="name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-2 col-form-label">Foto</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="inputPassword3" placeholder="Foto" @change="getImg" accept="image/*">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </form> 
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline-primary" @click.prevent="updateData('{!! base64_encode($row->id) !!}')">Submit</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Nama Barang</th>
                    <th>Foto</th>
                    <th>Dibuat Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
    <!-- /.card -->
    <div class="modal fade" id="modal-primary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Barang</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- form start -->
                    <form class="form-horizontal" enctype="multipart/form-data" @submit.prevent="storeData">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Masukkan Nama Barang" v-model="form.name" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Foto</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="inputPassword3" placeholder="Foto" @change="getImg" accept="image/*" name="image">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form> 
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary" @click.prevent="storeData">Submit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
@stop
@section('script')
<!-- DataTables  & Plugins -->
<script src="{!! asset('plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('plugins/jszip/jszip.min.js') !!}"></script>
<script src="{!! asset('plugins/pdfmake/pdfmake.min.js') !!}"></script>
<script src="{!! asset('plugins/pdfmake/vfs_fonts.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('plugins/datatables-buttons/js/buttons.colVis.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        new Vue({
            el: "#app",
            data() {
                return {
                    form: {
                        update: {
                            name: document.querySelector("input[name=name]").value
                        }
                    },
                    image: []
                }
            },
            mounted() {
                console.log('Vue is Running')
            },
            methods: {
                getImg(ev) {
                    this.image = ev.target.files[0];
                },
                storeData: function () {
                    const data = new FormData();
                    data.append('image', this.image);
                    data.append('name', this.form.name);
                    axios.post("{{ route('stuff.store') }}", data)
                        .then((res) => {
                            if (!res.data.error) {
                                Swal.fire("Berhasil", res.data.message, "success")
                                setTimeout(() => window.location.reload(), 2000)
                            }
                        })
                        .catch((err) => {
                            Swal.fire('Terjadi Kesalahan', err.response.data.message, "error")
                            setTimeout(() => window.location.reload(), 2000)
                        });
                },
                deleteData: function (id) {
                    Swal.fire({
                        title: 'Peringatan',
                        text: "Data tidak dapat kembali jika sudah dihapus",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Hapus'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            axios.delete("stuff/" + id)
                            .then((res) => {
                                if (!res.data.error) {
                                    Swal.fire("Berhasil", res.data.message, "success")
                                    setTimeout(() => window.location.reload(), 2000)
                                }
                            })
                            .catch((err) => {
                                Swal.fire('Terjadi Kesalahan', err.response.data.message, "error")
                                setTimeout(() => window.location.reload(), 2000)
                            })
                        }
                    })
                },
                updateData: function (id) { 
                    const data = new FormData();
                    data.append('image', this.image);
                    data.append('name', this.form.update.name);
                    data.append('_method', 'PATCH');
                    axios.post("stuff/" + id, data)
                        .then((res) => {
                            if (!res.data.error) {
                                Swal.fire("Berhasil", res.data.message, "success")
                                setTimeout(() => window.location.reload(), 2000)
                            }
                        })
                        .catch((err) => {
                            Swal.fire('Terjadi Kesalahan', err.response.data.message, "error")
                            setTimeout(() => window.location.reload(), 2000)
                        });
                }
            }
        })
    });
</script>
@stop